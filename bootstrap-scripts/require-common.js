var NOTIFY_MODULES = ['pnotify.buttons', 'pnotify.callbacks', 'pnotify.mobile', 'pnotify.animate'];
require.config({
    baseUrl: site.baseurl + '/js/',
    // Todo SIP (Subresource Integrity Protection)
    paths: {
        'babel-polyfill': 'https://cdnjs.cloudflare.com/ajax/libs/babel-polyfill/6.13.0/polyfill.min',
        'jquery': 'https://code.jquery.com/jquery-2.2.4.min',
        'bootstrap': 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min',
        'firebase-global': 'https://www.gstatic.com/firebasejs/3.3.0/firebase',
        'pnotify': '../bootstrap-scripts/pnotify.custom',
        'react': 'https://cdnjs.cloudflare.com/ajax/libs/react/15.3.1/react.min',
        'react-dom': 'https://cdnjs.cloudflare.com/ajax/libs/react/15.3.1/react-dom.min',
        'react-bootstrap': 'https://cdnjs.cloudflare.com/ajax/libs/react-bootstrap/0.30.3/react-bootstrap.min',
        'text': 'https://cdnjs.cloudflare.com/ajax/libs/require-text/2.0.12/text.min'
    },
    bundles: {
        'pnotify': NOTIFY_MODULES
    }
});
define('pnotify-all', ['pnotify'].concat(NOTIFY_MODULES));
define('firebase', ['firebase-global'], function () {
    return window.firebase;
});
// Require babel-polyfill before anything else.
require(['babel-polyfill'], function onPolyfillLoaded() {
    require(['jquery'], function onJQLoaded() {
        require(['firebase-setup', 'bootstrap'], function () {
            require(['common']);
        });
    });
});