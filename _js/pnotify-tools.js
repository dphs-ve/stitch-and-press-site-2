import $ from "jquery";
import "pnotify-all";
import PNotifty from "pnotify";

PNotifty.prototype.options.styling = "bootstrap3";
PNotifty.prototype.options.stack = {"dir1": "down", "dir2": "right"};

const settings = {
    addclass: 'stack-topleft',
    animate: {
        animate: true,
        in_class: 'flipInX',
        out_class: 'flipOutX'
    },
    animate_speed: "slow",
    shadow: false,
    icon: false,
    delay: 2000,
    buttons: {
        sticker: false,
        closer_hover: false
    }
};

export function noticeNoOpen(type, title, message, opts = {}) {
    opts = $.extend(true, {}, opts);
    opts.auto_display = false;
    return notice(type, title, message, opts);
}

export function notice(type, title, message, opts = {}) {
    window.lastMessage = message;
    const localSettings = $.extend(true, {
        type: type,
        title: title,
        text: message,
    }, settings);
    return new PNotifty($.extend(true, localSettings, opts));
}

export function createProgressNotice(type, title) {
    return notice(type, title, `
<div class="progress progress-striped active" style="margin:0">
   <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0">
        <span class="sr-only">0%</span>
    </div>
</div>`, {
        hide: false,
        insert_brs: false,
        buttons: {
            sticker: false,
            closer: false
        }
    });
}

export function updateProgressNotice(notice, value) {
    notice = notice.get().find("div.progress-bar");
    notice.width(value + "%").attr("aria-valuenow", value).find("span").html(value + "%");
}

export function closeProgressNotice(notice) {
    notice.remove();
}

export function replaceProgressNotice(notice, unopenedReplace) {
    notice.attention("rubberBand");
    notice.update(unopenedReplace.options);
}