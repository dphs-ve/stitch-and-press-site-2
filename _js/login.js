import $ from "jquery";
import firebase from "firebase";
import {noticeNoOpen, notice, createProgressNotice, updateProgressNotice, replaceProgressNotice} from "pnotify-tools";
import loginModalHtml from "text!../dialogs/loginModal.html";
import {resolveErrorToMessage} from "util";

const SIGN_IN_TXT = 'Sign In';
const SIGN_OUT_TXT = 'Sign Out';

function isLogIn($btn) {
    return $btn.text() === SIGN_IN_TXT;
}

let submissionInProgress = false;

function handleLoginSubmit(e, $form) {
    e.preventDefault();

    if (submissionInProgress) {
        return Promise.resolve();
    }
    submissionInProgress = true;

    const email = $form.find('#email').val();
    const pass = $form.find('#password').val();
    const bar = createProgressNotice("info", "Signing you in...");
    // Show some action for the users...
    updateProgressNotice(bar, 50);
    function finishProgressBar(val, notice) {
        updateProgressNotice(bar, val);
        setTimeout(() => replaceProgressNotice(bar, notice), 250);
    }

    return firebase.auth().signInWithEmailAndPassword(email, pass).then((auth) => {
        submissionInProgress = false;
        finishProgressBar(100, noticeNoOpen("success", "Signed In", "You are now signed in as " + auth.email));
        return auth;
    }).catch((e) => {
        submissionInProgress = false;
        finishProgressBar(0, noticeNoOpen("error", "Couldn't sign in", resolveErrorToMessage(e), {
            delay: 5000
        }));
        throw e;
    });
}

function showLoginScreen() {
    let loginBody = $(loginModalHtml).appendTo("body");
    loginBody.find("#login-form")
        .on("submit", (e) =>
            handleLoginSubmit(e, $(e.target))
                .then(() => loginBody.modal('hide')));
    loginBody.modal({
        backdrop: "static"
    });
}

function handleClick($btn) {
    if (isLogIn($btn)) {
        showLoginScreen();
    } else {
        firebase.auth().signOut().then(() => {
            notice("success", "Signed Out", "You have successfully signed out.");
        }).catch((e) => {
            notice("error", "Couldn't sign out", resolveErrorToMessage(e));
        });
    }
}


const $btn = $("#login-change-button");
$btn.click((e) => {
    e.preventDefault();
    handleClick($btn);
});
function updateSignInStatus() {
    let text;
    if (firebase.auth().currentUser !== null) {
        text = SIGN_OUT_TXT;
    } else {
        text = SIGN_IN_TXT;
    }
    $btn.text(text);
}
firebase.auth().onAuthStateChanged(updateSignInStatus);