import $ from "jquery";
import React from "react";
import ReactDOM from "react-dom";
const $store = $("#store");
const data = ["Product 1", "Product 2", "Product 3",
    <span className="glyphicon glyphicon-resize-horizontal">MAXIMUM SPANNING TREE</span>];
class ProductList extends React.Component {
    render() {
        return <ul className="list-group">
            {data.map((d, i) => <li className="list-group-item" key={i}>{d}</li>)}
        </ul>;
    }
}
ReactDOM.render(<ProductList/>, $store[0]);